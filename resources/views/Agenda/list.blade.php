@extends('main.main')

@section('content')
    <div class="container">
        @if(session('msg'))
            <span class="alert alert-success">
                {{session('msg')}}
            </span>
        @endif
    <table class="table table-bordered table-responsive">
        <thead>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach($contatos as $contato)
                <tr>
                    <td>{{$contato->nome}}</td>
                    <td>{{$contato->email}}</td>
                    <td>
                        <a class="btn btn-info" href="{{route('agenda.show', $contato->id)}}">Detalhes</a>
                        <a class="btn btn-warning" href="javascript:$('#edit-contato').modal('show')" data-toggle="modal" data-target=".edit-{{$contato->id}}">Editar</a>
                        <a class="btn btn-danger" href="">remover</a>

                        <div class="modal fade edit-{{$contato->id}}" id="edit-contato">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        @include('agenda.edit')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </div>
@stop