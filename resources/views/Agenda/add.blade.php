@extends('main.main')

@section('content')
    <div class="container">
        <form class="form-horizontal" action="{{route('store')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <div class="col-sm-12">
                    <label>Nome:</label>
                    <input class="form-control" type="text" name="nome">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    <label>E-mail:</label>
                    <input class="form-control" type="text" name="email" id="">
                </div>
                <div class="col-sm-4">
                    <labele>Telefone Fixo</labele>
                    <input class="form-control" type="text" name="telefone" id="">
                </div>
                <div class="col-sm-4">
                    <labele>Celular:</labele>
                    <input class="form-control" type="text" name="telefone2" id="">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label>Facebook</label>
                    <input class="form-control" type="text" name="facebook">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <input class="btn btn-block btn-success" type="submit" value="Salvar">
                </div>
            </div>
        </form>
    </div>
@stop