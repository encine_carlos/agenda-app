<div>
    <form class="form-horizontal" action="{{route('agenda.update', $contato->id)}}" method="post">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group">
            <div class="col-sm-12">
                <label>Nome:</label>
                <input class="form-control" type="text" name="nome" value="{{$contato->nome}}">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label>E-mail:</label>
                <input class="form-control" type="text" name="email" value="{{$contato->email}}">
            </div>
            <div class="col-sm-4">
                <labele>Telefone Fixo</labele>
                <input class="form-control" type="text" name="telefone" value="{{$contato->telefone}}">
            </div>
            <div class="col-sm-4">
                <labele>Celular:</labele>
                <input class="form-control" type="text" name="telefone2" value="{{$contato->telefone2}}">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <label>Facebook</label>
                <input class="form-control" type="text" name="facebook" value="{{$contato->facebook}}">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <input class="btn btn-block btn-success" type="submit" value="Salvar">
            </div>
        </div>
    </form>
</div>
