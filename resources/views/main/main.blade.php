<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Agenda de contatos</title>
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="navbar-header">
            <a href="#" class="navbar-brand">Agenda</a>
        </div>
    </nav>
    @yield('content')

    <script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

</body>
</html>